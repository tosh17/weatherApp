package ru.thstdio.geek.weatherapp.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.RemoteViews
import ru.thstdio.geek.weatherapp.R
import ru.thstdio.geek.weatherapp.ServiceWeatherLoader
import ru.thstdio.geek.weatherapp.database.DbHelper
import ru.thstdio.geek.weatherapp.database.DbWorker


/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in [WeatherWidgetConfigureActivity]
 */
class WeatherWidget : AppWidgetProvider() {
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId, true)
        }
    }

    override fun onDeleted(context: Context, appWidgetIds: IntArray) {
        // When the user deletes the widget, delete the preference associated with it.
        for (appWidgetId in appWidgetIds) {
            WeatherWidgetConfigureActivity.deleteTitlePref(context, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {

        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int, onUpdate: Boolean) {

            val weatherSettings = WeatherWidgetConfigureActivity.loadWidgetSettings(context, appWidgetId)
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.weather_widget)
            val dbWorker = DbWorker(DbHelper(context))
            val weather = dbWorker.cityWeatherLoad(weatherSettings.city)
            views.setTextViewText(R.id.textTitle, weatherSettings.city)
            views.setTextViewText(R.id.temperTxtInfo, context.getString(R.string.txtInfo_temp, weather.temp.toString()))
            if (weatherSettings.isHumidityShow) {
                views.setViewVisibility(R.id.humidityTxtInfo, View.VISIBLE)
                views.setTextViewText(R.id.humidityTxtInfo, context.getString(R.string.txtInfo_humidity, weather.humidity.toString()))
            } else {
                views.setViewVisibility(R.id.humidityTxtInfo, View.GONE)
            }
            if (weatherSettings.isWindSpeedShow) {
                views.setViewVisibility(R.id.windTxtInfo, View.VISIBLE)
                views.setTextViewText(R.id.windTxtInfo, context.getString(R.string.txtInfo_wind, weather.windSpeed.toString()))
            } else {
                views.setViewVisibility(R.id.windTxtInfo, View.GONE)
            }

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
            if (onUpdate) {
                val intent = Intent(context, ServiceWeatherLoader::class.java)
                intent.putExtra(ServiceWeatherLoader.COMMAND_KEY_CITY, weatherSettings.city)
                intent.putExtra(ServiceWeatherLoader.COMMAND_KEY_WIDGET_ID, appWidgetId)
                context.startService(intent)
            }
        }

    }

    override fun onReceive(context: Context?, intent: Intent?) {

        super.onReceive(context, intent)
        intent?.let {
            context?.let {
                val extras = intent.extras
                if (extras != null) {
                    val appWidgetManager = AppWidgetManager.getInstance(context)
                    //    val thisAppWidget = ComponentName(context.getPackageName(), WeatherWidget::class.java!!.getName())

                    val appWidgetId = extras[ServiceWeatherLoader.COMMAND_KEY_WIDGET_ID]

                    if (appWidgetId != null && appWidgetId is Int)
                        updateAppWidget(context, appWidgetManager, appWidgetId, false)
                }
            }
        }
    }
}


