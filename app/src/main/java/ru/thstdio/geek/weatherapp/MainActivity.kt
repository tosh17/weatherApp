package ru.thstdio.geek.weatherapp

import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content.*
import ru.thstdio.geek.weatherapp.database.DbHelper
import ru.thstdio.geek.weatherapp.database.DbWorker
import ru.thstdio.geek.weatherapp.ui.CitySelectDialog
import ru.thstdio.geek.weatherapp.util.TripleDES
import ru.thstdio.geek.weatherapp.util.WeatherObject


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, ServiceWeatherLoader.OnWeatherLoader {
    // private val handler = Handler()
    private var prefs: SharedPreferences? = null
    private val FILENAME = "ru.thstdio.geek.weather.prefs"
    private val SETTINGS_CITY = "SETTINGS_CITY"

    private lateinit var sConn: ServiceConnection
    private lateinit var service: ServiceWeatherLoader
    private var bind = false
    fun updateWeatherData(city: String) {
//        Thread(Runnable {
//            val json = WeatherDataLoader.getJSONData(applicationContext, city)
//            if (json == null) {
//                handler.post {
//                    Toast.makeText(applicationContext, getString(R.string.place_not_found), Toast.LENGTH_LONG).show()
//                }
//            } else {
//                handler.post {
//                    val weather = WeatherObject()
//                    weather.fromJson(json)
//                    saveCity(city, weather)
//                    printCityInfo(weather)
//                }
//            }
//
//        }).start()
        service.loadWetherCity(city)
    }

    private var currentWeather: WeatherObject? = null

    private fun printCityInfo(weather: WeatherObject) {
        currentWeather = weather
        textTitle.text = weather.city
        temperTxtInfo.text = getString(R.string.txtInfo_temp, weather.temp.toString())
        humidityTxtInfo.text = getString(R.string.txtInfo_humidity, weather.humidity.toString())
        windTxtInfo.text = getString(R.string.txtInfo_wind, weather.windSpeed.toString())
        imageWindTxtInfo.rotation = (weather.windDeg - 90).toFloat()
    }


    private var isPressureShow = true
    private var isWindSpeedShow = true
    private var isHumidityShow = true

    private lateinit var dbWorker: DbWorker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        dbWorker = DbWorker(DbHelper(this))
        floatingActionButton.setOnClickListener(this)
        sConn = object : ServiceConnection {

            override fun onServiceConnected(name: ComponentName, binder: IBinder) {
                service = (binder as ServiceWeatherLoader.WeatherBinder).getService()
                service.onWeatherLoader = this@MainActivity
                bind = true
                loadlastCity()
            }

            override fun onServiceDisconnected(name: ComponentName) {
                bind = false
            }
        }

        val intent = Intent(baseContext, ServiceWeatherLoader::class.java)
        bindService(intent, sConn, Context.BIND_AUTO_CREATE)


    }

    override fun onStart() {
        super.onStart()
        if (prefs == null) prefs = this.getSharedPreferences(FILENAME, 0)

    }

    fun loadlastCity() {
        val lastCityEncrypt = prefs!!.getString(SETTINGS_CITY, "")
        if (lastCityEncrypt == "") return
        val lastCity = TripleDES.decrypt(TripleDES.stringToByte(lastCityEncrypt), "qwerty")
        updateWeatherData(lastCity)
    }

    private fun saveCity(city: String, weather: WeatherObject) {
        if (prefs == null) prefs = this.getSharedPreferences(FILENAME, 0)
        val editor = prefs!!.edit()
        editor.putString(SETTINGS_CITY, TripleDES.byteToString(TripleDES.encrypt(city, "qwerty")))
        editor.apply()
        if (dbWorker.addCity(city)) dbWorker.addWeather(dbWorker.findCity(city), weather)
        else dbWorker.upgradeWeather(dbWorker.findCity(city), weather)

    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_action_bar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_select_city -> showSelectCitydialog()
            R.id.action_settings_show_pressure -> {
                isPressureShow = !isPressureShow
                item.isCheckable = isPressureShow
                toastMe("pressure $isPressureShow")

            }
            R.id.action_settings_show_wind_speed -> {
                isWindSpeedShow = !isWindSpeedShow
                item.isCheckable = isWindSpeedShow
                toastMe("wind_speed $isWindSpeedShow")
            }
            R.id.action_settings_show_humidity -> {
                isHumidityShow = !isHumidityShow
                item.isCheckable = isHumidityShow
                toastMe("wind_speed $isHumidityShow")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        menuInflater.inflate(R.menu.menu_context, menu)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_magner -> {
            }
            R.id.nav_wind -> {

            }
            R.id.nav_developer -> {

            }
            R.id.nav_feedback -> {

            }
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onClick(p0: View?) {

        when (p0) {
            floatingActionButton -> {
                currentWeather?.let {
                    intent = Intent(Intent.ACTION_SEND)
                    intent.putExtra(Intent.EXTRA_TEXT, currentWeather.toString())
                    intent.type = "text/plain"
                    startActivity(intent)
                }
            }
        }
    }

    override fun onPause() {
        if (bind) unbindService(sConn)
        super.onPause()
    }

    override fun onDestroy() {
        //unbindService(sConn)
        super.onDestroy()
    }

    private fun showSelectCitydialog() {
        val dialog = CitySelectDialog()
        val list = dbWorker.listCity()
        val bundle = Bundle()
        bundle.putStringArrayList(CitySelectDialog.CITY_LIST, list)
        dialog.arguments = bundle
        val manager = supportFragmentManager
        dialog.show(manager, "dialog")
    }

    private fun toastMe(str: String) {
        //Toast.makeText(this, str, Toast.LENGTH_SHORT).show()
        Snackbar.make(floatingActionButton, str, Snackbar.LENGTH_LONG).show()

    }

    override fun loadComplete(city: String) {
        printCityInfo(dbWorker.cityWeatherLoad(city))
    }

}
