package ru.thstdio.geek.weatherapp.database

import android.content.ContentValues
import android.util.Log
import ru.thstdio.geek.weatherapp.util.WeatherObject

class DbWorker(private val dbHelper: DbHelper) : WeatherBD {
    override fun cityWeatherLoad(city: String): WeatherObject {
        val cityId = findCity(city)
        val db = dbHelper.writableDatabase
        val selectQuery = "SELECT  * FROM '${WeatherCityTable.TABLE_NAME}' WHERE ${WeatherCityTable.ID} = \'$cityId\'"
        val cursor = db.rawQuery(selectQuery, null)
        val weatherObject = WeatherObject()
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                weatherObject.city = city
                weatherObject.temp = cursor.getDouble(cursor.getColumnIndex(WeatherCityTable.TEMP))
                weatherObject.humidity = cursor.getDouble(cursor.getColumnIndex(WeatherCityTable.HUMIDITY))
                weatherObject.windSpeed = cursor.getDouble(cursor.getColumnIndex(WeatherCityTable.WIND_SPEED))
                weatherObject.windDeg = cursor.getDouble(cursor.getColumnIndex(WeatherCityTable.WIND_DEG))
            }
        }
        cursor.close()
        db.close()
        return weatherObject
    }

    override fun addWeather(cityId: Int, weather: WeatherObject): Boolean {
        val db = dbHelper.writableDatabase
        val values = WeatherCityTable.getContentValues(cityId, weather)
        val success = db.insert(WeatherCityTable.TABLE_NAME, null, values)
        db.close()
        Log.v("InsertedId", "$success")
        return (Integer.parseInt("$success") != -1)
    }

    override fun upgradeWeather(cityId: Int, weather: WeatherObject): Boolean {
        val db = dbHelper.writableDatabase
        val values = WeatherCityTable.getContentValues(cityId, weather)
        val success = db.update(WeatherCityTable.TABLE_NAME, values, WeatherCityTable.ID + "=?", arrayOf(cityId.toString())).toLong()
        db.close()
        return Integer.parseInt("$success") != -1
    }

    override fun findCity(city: String): Int {
        var cityId = 0
        val db = dbHelper.writableDatabase
        val selectQuery = "SELECT  * FROM '${CityTable.TABLE_NAME}' WHERE ${CityTable.CITY} = \'$city\'"
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                cityId = cursor.getInt(cursor.getColumnIndex(CityTable.ID))
            }
        }
        cursor.close()
        db.close()
        return cityId

    }

    override fun addCity(city: String): Boolean {
        if (findCity(city) > 0) return false
        val db = dbHelper.writableDatabase
        val values = ContentValues()
        values.put(CityTable.CITY, city)
        val success = db.insert(CityTable.TABLE_NAME, null, values)
        db.close()
        Log.v("InsertedId", "$success")
        return (Integer.parseInt("$success") != -1)
    }

    override fun listCity(): ArrayList<String> {
        val listCity = ArrayList<String>()
        val db = dbHelper.writableDatabase
        val selectQuery = "SELECT  * FROM '${CityTable.TABLE_NAME}'"
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val city = cursor.getString(cursor.getColumnIndex(CityTable.CITY))
                    listCity.add(city)
                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        return listCity
    }

}