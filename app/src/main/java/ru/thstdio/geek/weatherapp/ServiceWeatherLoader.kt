package ru.thstdio.geek.weatherapp

import android.app.Service
import android.appwidget.AppWidgetManager.ACTION_APPWIDGET_UPDATE
import android.content.Intent
import android.content.ServiceConnection
import android.os.AsyncTask
import android.os.Binder
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import ru.thstdio.geek.weatherapp.database.DbHelper
import ru.thstdio.geek.weatherapp.database.DbWorker
import ru.thstdio.geek.weatherapp.net.WeatherDataLoader.Companion.getJSONData
import ru.thstdio.geek.weatherapp.util.WeatherObject

class ServiceWeatherLoader : Service() {
    companion object {
        val COMMAND_KEY_CITY = "COMMAND_KEY_CITY"
        val COMMAND_KEY_WIDGET_ID = "COMMAND_KEY_WIDGET_ID"
    }

    var binder = WeatherBinder()
    lateinit var onWeatherLoader: OnWeatherLoader

    override fun onBind(intent: Intent): IBinder {
        Toast.makeText(this, "onBind", Toast.LENGTH_SHORT).show()
        return binder

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "service starting", Toast.LENGTH_LONG).show()
        intent?.let {
            val city = intent.extras[COMMAND_KEY_CITY]
            val widgetId = intent.extras[COMMAND_KEY_WIDGET_ID]
            val ex = AsynComandLoad(widgetId as Int)
            ex.execute(city as String?)
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun unbindService(conn: ServiceConnection?) {
        super.unbindService(conn)
        Toast.makeText(this, "unbindService", Toast.LENGTH_SHORT).show()
    }

    fun loadWetherCity(city: String) {
        AsynLoad().execute(city)
    }

    inner class AsynLoad() : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg p0: String?): String? {
            val dbWorker = DbWorker(DbHelper(applicationContext))
            val city = p0[0]
            city?.let {
                val json = getJSONData(applicationContext, city)
                val weather = WeatherObject()
                weather.fromJson(json)
                if (dbWorker.addCity(city)) dbWorker.addWeather(dbWorker.findCity(city), weather)
                else dbWorker.upgradeWeather(dbWorker.findCity(city), weather)
            }
            return p0[0]
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            result?.let { onWeatherLoader.loadComplete(result) }

        }
    }

    inner class AsynComandLoad(var widgetId: Int) : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg p0: String?): String? {
            val dbWorker = DbWorker(DbHelper(applicationContext))
            val city = p0[0]
            city?.let {
                val json = getJSONData(applicationContext, city)
                val weather = WeatherObject()
                weather.fromJson(json)
                if (dbWorker.addCity(city)) dbWorker.addWeather(dbWorker.findCity(city), weather)
                else dbWorker.upgradeWeather(dbWorker.findCity(city), weather)
            }
            return p0[0]
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            val intent = Intent(ACTION_APPWIDGET_UPDATE)
            intent.putExtra(ServiceWeatherLoader.COMMAND_KEY_CITY, result)
            intent.putExtra(ServiceWeatherLoader.COMMAND_KEY_WIDGET_ID, widgetId)
            Log.d("Test", widgetId.toString())
            sendBroadcast(intent)
        }

    }


    interface OnWeatherLoader {
        fun loadComplete(city: String)
    }

    inner class WeatherBinder : Binder() {
        fun getService(): ServiceWeatherLoader {
            return this@ServiceWeatherLoader
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "onServiceDestroy", Toast.LENGTH_LONG).show()
    }
}
