package ru.thstdio.geek.weatherapp.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbHelper(context: Context) : SQLiteOpenHelper(context, DbHelper.DB_NAME, null, DbHelper.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        db?.let {
            CityTable.onCreate(db)
            WeatherCityTable.onCreate(db)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.let {
            CityTable.onUpgrade(db, oldVersion, newVersion)
            WeatherCityTable.onUpgrade(db, oldVersion, newVersion)
        }

    }

    companion object {
        private const val DB_VERSION = 1
        private const val DB_NAME = "WeatherDB"
    }
}