package ru.thstdio.geek.weatherapp.util

import org.json.JSONObject

class WeatherObject {
    var city: String = ""
    private var main: String = ""
    private var description: String = ""
    var temp: Double = 0.0
    private var tempMax: Double = 0.0
    private var tempMin: Double = 0.0
    var humidity: Double = 0.0
    var windSpeed: Double = 0.0
    var windDeg: Double = 0.0

    fun fromJson(jsonObj: JSONObject?) {
        jsonObj?.let {
            val jsonWeatherArray = jsonObj.getJSONArray("weather")[0] as JSONObject
            val jsonMainObj = jsonObj.getJSONObject("main")
            val jsonWindObj = jsonObj.getJSONObject("wind")
            city = jsonObj.get("name").toString()
            main = jsonWeatherArray.toString()
            description = jsonWeatherArray.get("description").toString()
            temp = jsonMainObj.get("temp").toString().toDouble()
            tempMax = jsonMainObj.get("temp_max").toString().toDouble()
            tempMin = jsonMainObj.get("temp_min").toString().toDouble()
            humidity = jsonMainObj.get("humidity").toString().toDouble()
            windSpeed = jsonWindObj.get("speed").toString().toDouble()
            windDeg = jsonWindObj.get("deg").toString().toDouble()
        }

    }

    override fun toString(): String {
        return "city='$city'\n description='$description'\ntemp='${out(temp)}'\ntempMax='${out(tempMax)}'\ntempMin=${out(tempMin)}\nhumidity=${out(humidity)}\nwindSpeed=${out(windSpeed)}\nwindDeg=${out(windDeg)}"
    }

    private fun out(a: Double): String {
        val s = a.toString().split('.')[1].toDouble()
        return if (s > 0) {
            a.toString()
        } else {
            a.toInt().toString()
        }
    }

}