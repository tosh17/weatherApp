package ru.thstdio.geek.weatherapp.ui

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_city_select.*
import kotlinx.android.synthetic.main.item_dialog_select_select.view.*
import ru.thstdio.geek.weatherapp.MainActivity
import ru.thstdio.geek.weatherapp.R

class CitySelectDialog : DialogFragment(), ItemClick {

    companion object {
        const val CITY_LIST = "CITY_LIST"
    }

    lateinit var list: ArrayList<String>
    override fun itemClick(position: Int) {
        commitCity(list.get(position))
    }

    lateinit var content: RecyclerView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(getString(R.string.select_city))
        builder.setView(R.layout.dialog_city_select)
        builder.setPositiveButton("Ok", { dialogInterface: DialogInterface, i: Int ->
            commitCity("")
        })

        return builder.create()

    }

    override fun onStart() {
        super.onStart()
        list = arguments.get(CITY_LIST) as ArrayList<String>
        content = dialog!!.findViewById(R.id.contentDialogCitySelect)
        content.layoutManager = LinearLayoutManager(activity)
        content.adapter = DialogAdapter(list, this)
    }

    fun commitCity(city: String) {
        val result: String
        if (city == "") result = dialog!!.editTextCityName.text.toString()
        else result = city
        val act = activity as MainActivity
        act.updateWeatherData(result)
        dialog.cancel()
    }

    class DialogAdapter(val list: ArrayList<String>, val click: ItemClick) : RecyclerView.Adapter<DialogHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DialogHolder {
            val inflatedView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_dialog_select_select, null)
            val holder = DialogHolder(inflatedView, click)
            return holder
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: DialogHolder?, position: Int) {
            holder?.let {
                holder.bind(list[position])
            }

        }

    }

    class DialogHolder(private val v: View, val click: ItemClick) : RecyclerView.ViewHolder(v), View.OnClickListener {
        init {
            v.setOnClickListener(this)
        }

        fun bind(city: String) {
            v.txtCity.text = city
        }

        override fun onClick(p0: View?) {
            click.itemClick(adapterPosition)
        }
    }

}

