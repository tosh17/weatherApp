package ru.thstdio.geek.weatherapp.database

import ru.thstdio.geek.weatherapp.util.WeatherObject

interface WeatherBD {
    fun addCity(city: String): Boolean
    fun findCity(city: String): Int
    fun cityWeatherLoad(city: String): WeatherObject
    fun listCity(): ArrayList<String>
    fun addWeather(cityId: Int, weather: WeatherObject): Boolean
    fun upgradeWeather(cityId: Int, weather: WeatherObject): Boolean
}