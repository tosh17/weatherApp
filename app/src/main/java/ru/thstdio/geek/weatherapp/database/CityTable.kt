package ru.thstdio.geek.weatherapp.database

import android.database.sqlite.SQLiteDatabase

class CityTable {
    companion object {
        const val TABLE_NAME = "CityTable"
        const val ID = "Id"
        const val CITY = "Name"
        fun onCreate(db: SQLiteDatabase) {
            val query = "CREATE TABLE $TABLE_NAME ($ID INTEGER PRIMARY KEY, $CITY TEXT);"
            db.execSQL(query)
        }

        fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            if (newVersion > oldVersion) {
                val query = "DROP TABLE IF EXISTS $TABLE_NAME"
                db.execSQL(query)
                onCreate(db)
            }
        }
    }

}