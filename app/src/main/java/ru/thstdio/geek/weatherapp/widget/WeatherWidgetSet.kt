package ru.thstdio.geek.weatherapp.widget

class WeatherWidgetSet(val city: String, val isHumidityShow: Boolean, val isWindSpeedShow: Boolean)