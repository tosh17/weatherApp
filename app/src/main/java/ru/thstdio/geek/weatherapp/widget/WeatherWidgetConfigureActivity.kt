package ru.thstdio.geek.weatherapp.widget

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText
import ru.thstdio.geek.weatherapp.R

/**
 * The configuration screen for the [WeatherWidget] AppWidget.
 */
class WeatherWidgetConfigureActivity : Activity() {
    private var mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID
    private lateinit var mAppWidgetCityText: EditText
    private lateinit var mAppWidgetHumidityCheckBox: CheckBox
    private lateinit var mAppWidgetWindSpeedyCheckBox: CheckBox


    public override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(Activity.RESULT_CANCELED)

        setContentView(R.layout.weather_widget_configure)
        mAppWidgetCityText = findViewById<View>(R.id.appwidget_text) as EditText
        mAppWidgetHumidityCheckBox = findViewById<View>(R.id.appwidget_checkBox_humidity) as CheckBox
        mAppWidgetWindSpeedyCheckBox = findViewById<View>(R.id.appwidget_checkBox_wind_speed) as CheckBox

        findViewById<View>(R.id.add_button).setOnClickListener({
            val context = this@WeatherWidgetConfigureActivity
            val widgetText = mAppWidgetCityText.text.toString()
            saveWidgetSettings(context, mAppWidgetId, widgetText, mAppWidgetHumidityCheckBox.isChecked, mAppWidgetWindSpeedyCheckBox.isChecked)

            // It is the responsibility of the configuration activity to update the app widget
            val appWidgetManager = AppWidgetManager.getInstance(context)
            WeatherWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId, false)

            // Make sure we pass back the original appWidgetId
            val resultValue = Intent()
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId)
            setResult(Activity.RESULT_OK, resultValue)
            finish()
        })

        // Find the widget id from the intent.
        val intent = intent
        val extras = intent.extras
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID)
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
            return
        }
        val weatherSettings = loadWidgetSettings(this@WeatherWidgetConfigureActivity, mAppWidgetId)
        mAppWidgetCityText.setText(weatherSettings.city)
        mAppWidgetHumidityCheckBox.isChecked = weatherSettings.isHumidityShow
        mAppWidgetWindSpeedyCheckBox.isChecked = weatherSettings.isWindSpeedShow
    }

    companion object {

        private const val PREFS_NAME = "ru.thstdio.geek.weatherapp.widget.WeatherWidget"
        private const val PREF_CITY_KEY = "appwidget_city_"
        private const val PREF_HUMIDITY_KEY = "appwidget_humidity_"
        private const val PREF_WIND_SPEED_KEY = "appwidget_wind_speed_"

        // Write the prefix to the SharedPreferences object for this widget
        internal fun saveWidgetSettings(context: Context, appWidgetId: Int, text: String, isHumidityShow: Boolean, isWindSpeedShow: Boolean) {
            val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
            prefs.putString(PREF_CITY_KEY + appWidgetId, text)
            prefs.putBoolean(PREF_HUMIDITY_KEY + appWidgetId, isHumidityShow)
            prefs.putBoolean(PREF_WIND_SPEED_KEY + appWidgetId, isWindSpeedShow)
            prefs.apply()
        }

        // Read the prefix from the SharedPreferences object for this widget.
        // If there is no preference saved, get the default from a resource
        internal fun loadWidgetSettings(context: Context, appWidgetId: Int): WeatherWidgetSet {
            val prefs = context.getSharedPreferences(PREFS_NAME, 0)
            val city = prefs.getString(PREF_CITY_KEY + appWidgetId, "")
            val isHumidityShow = prefs.getBoolean(PREF_HUMIDITY_KEY + appWidgetId, true)
            val isWindSpeedShow = prefs.getBoolean(PREF_WIND_SPEED_KEY + appWidgetId, true)
            return WeatherWidgetSet(city, isHumidityShow, isWindSpeedShow)
        }

        internal fun deleteTitlePref(context: Context, appWidgetId: Int) {
            val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
            prefs.remove(PREF_CITY_KEY + appWidgetId)
            prefs.remove(PREF_HUMIDITY_KEY + appWidgetId)
            prefs.remove(PREF_WIND_SPEED_KEY + appWidgetId)
            prefs.apply()
        }
    }
}

