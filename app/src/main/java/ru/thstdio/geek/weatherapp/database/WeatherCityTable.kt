package ru.thstdio.geek.weatherapp.database

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import ru.thstdio.geek.weatherapp.util.WeatherObject

class WeatherCityTable {
    companion object {
        const val TABLE_NAME = "WeatherCityTable"
        const val ID = "Id"
        const val TEMP = "Temp"
        const val HUMIDITY = "Humidity"
        const val WIND_SPEED = "WindSpeed"
        const val WIND_DEG = "WindDeg"
        fun onCreate(db: SQLiteDatabase) {
            val query = "CREATE TABLE $TABLE_NAME ($ID INTEGER PRIMARY KEY, $TEMP REAL, $HUMIDITY REAL, $WIND_SPEED REAL, $WIND_DEG REAL);"
            db.execSQL(query)
        }

        fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            if (newVersion > oldVersion) {
                val query = "DROP TABLE IF EXISTS $TABLE_NAME"
                db.execSQL(query)
                onCreate(db)
            }
        }

        fun getContentValues(id: Int, weather: WeatherObject): ContentValues {
            val values = ContentValues()
            values.put(ID, id)
            values.put(TEMP, weather.temp)
            values.put(HUMIDITY, weather.humidity)
            values.put(WIND_SPEED, weather.windSpeed)
            values.put(WIND_DEG, weather.windDeg)
            return values
        }
    }
}