package ru.thstdio.geek.weatherapp.util

import java.nio.charset.Charset
import java.security.MessageDigest
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object TripleDES {

    @Throws(Exception::class)
    fun encrypt(message: String, pin: String): ByteArray {
        val md = MessageDigest.getInstance("md5")
        //  final byte[] digestOfPassword = md.digest("HG58YZ3CR9"
        val digestOfPassword = md.digest(abc(pin)
                .toByteArray(charset("utf-8")))
        val keyBytes = Arrays.copyOf(digestOfPassword, 24)
        var j = 0
        var k = 16
        while (j < 8) {
            keyBytes[k++] = keyBytes[j++]
        }

        val key = SecretKeySpec(keyBytes, "DESede")
        val iv = IvParameterSpec(ByteArray(8))
        val cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, key, iv)

        val plainTextBytes = message.toByteArray(charset("utf-8"))
// final String encodedCipherText = new sun.misc.BASE64Encoder()
        // .encode(cipherText);

        return cipher.doFinal(plainTextBytes)
    }

    @Throws(Exception::class)
    fun decrypt(message: ByteArray, pin: String): String {
        val md = MessageDigest.getInstance("md5")
        //  final byte[] digestOfPassword = md.digest("HG58YZ3CR9"
        val digestOfPassword = md.digest(abc(pin)
                .toByteArray(charset("utf-8")))

        val keyBytes = Arrays.copyOf(digestOfPassword, 24)
        var j = 0
        var k = 16
        while (j < 8) {
            keyBytes[k++] = keyBytes[j++]
        }

        val key = SecretKeySpec(keyBytes, "DESede")
        val iv = IvParameterSpec(ByteArray(8))
        val decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding")
        decipher.init(Cipher.DECRYPT_MODE, key, iv)

        // final byte[] encData = new
        // sun.misc.BASE64Decoder().decodeBuffer(message);
        val plainText = decipher.doFinal(message)
        return String(plainText, Charset.defaultCharset())
        //return String(plainText, "UTF-8")
    }

    fun byteToString(bytes: ByteArray): String {
        val str = StringBuffer("")
        for (i in bytes.indices) str.append(bytes[i].toString() + ";")
        return str.toString()
    }

    fun stringToByte(str: String): ByteArray {
        val s = str.split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val bytes = ByteArray(s.size)
        var i = 0
        for (v in s) bytes[i++] = java.lang.Byte.parseByte(v)
        return bytes
    }

    private fun abc(pin: String): String {
        return pin[0] + "W" + pin[1] + "a" + pin[2] + "R" + pin[3] +
                "k" + pin[4] + "E" + pin[5]
    }
}