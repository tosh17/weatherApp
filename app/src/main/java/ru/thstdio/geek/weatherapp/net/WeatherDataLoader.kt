package ru.thstdio.geek.weatherapp.net

import android.content.Context
import org.json.JSONObject
import ru.thstdio.geek.weatherapp.R
import java.net.HttpURLConnection
import java.net.URL

class WeatherDataLoader {

    companion object {
        private const val OPEN_WEATHER_MAP_API =
                "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric"
        private const val KEY = "x-api-key"
        private const val RESPONSE = "cod"
        private const val ALL_GOOD = 200

        fun getJSONData(context: Context, city: String): JSONObject? {
            try {
                val url = URL(String.format(OPEN_WEATHER_MAP_API, city))
                val connection = url.openConnection() as HttpURLConnection
                connection.addRequestProperty(KEY,
                        context.getString(R.string.open_weather_maps_app_id))
                val data = connection.inputStream.bufferedReader().readText()
                val jsonObject = JSONObject(data)
                if (jsonObject.getInt(RESPONSE) != ALL_GOOD) {
                    return null
                }
                return jsonObject
            } catch (e: Exception) {
                e.printStackTrace()
                return null // FIXME Обработка ошибки
            }
        }
    }
}

