package ru.thstdio.geek.weatherapp.ui

interface ItemClick {
    fun itemClick(position: Int)
}